class Fixnum

  def in_words
    number_dictionary = { 1 => "one", 2 => "two", 3 => "three", 4 => "four", 5 => "five", 6 => "six", 7 => "seven", 8 => "eight", 9 => "nine", 10 => 'ten', 11 => "eleven", 12 => "twelve", 13 => "thirteen", 14 => "fourteen", 15 => "fifteen", 16 => "sixteen", 17 => "seventeen", 18 => "eighteen", 19 => "nineteen", 20 => "twenty", 30 => "thirty", 40 => "forty", 50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty", 90 => "ninety" }

    grouping_dictionary = {4 => "thousand", 7 => "million", 10 => "billion", 13 => "trillion" }
    digits = self.to_s.split("").map { |n| n.to_i }
    digits_word = ""

    digits.each_with_index do |digit, idx|

      place = (digits.size - idx) % 3
      return "zero" if self == 0
      case place
      when 0 #hundreds
        next if digit == 0
        digits_word << number_dictionary[digit] + " hundred "

      when 2 #tens
        next if digit == 0
        if digit == 1
          tens = digit * 10 + digits[idx + 1]
          digits_word << number_dictionary[tens] + " "
        else
          tens = digit * 10
          digits_word << number_dictionary[tens] + " "
        end
      when 1 #ones
        if (digits[idx - 1] == 1 && idx - 1 != -1) || digit == 0
          next if grouping_dictionary.value?(digits_word.split(" ").last)
        else
        digits_word << number_dictionary[digit] + " "
        end
      end
      grouping_idx = digits.size - idx
      next if grouping_dictionary.key?(grouping_idx) == false
      digits_word << grouping_dictionary[grouping_idx] + " "
    end #do

    digits_word.strip
  end #in_words
end #Fixnum
